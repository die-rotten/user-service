<?php

namespace App\Console\Commands;

use App\Role;
use App\Services\RegistrationService;
use Bschmitt\Amqp\Facades\Amqp;
use Illuminate\Console\Command;

class AmqpConsume extends Command
{
    /**
     * Signature console command.
     *
     * @var string
     */
    protected $signature = 'amqp:consume';

    /**
     * Description of command.
     *
     * @var string
     */
    protected $description = 'Start consuming AMQP message';

    /**
     * Create new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function handle(RegistrationService $registration_service)
    {
        Amqp::consume('user_service_queue', function ($message, $resolver) use ($registration_service) {
            $payload = json_decode($message->body, true);
            var_dump($message->body);
            $data = [
                'email' => $payload['email'],
                'password' => app('hash')->make($payload['password']),
                'role_id' => Role::AUTHOR,
                'personal' => [
                    'full_name' => $payload['personal']['full_name'],
                    'phone_number' => $payload['personal']['phone_number'],
                ],
            ];

            $user = $registration_service->register($data);

            if (!is_null($user)) {
                $resolver->acknowledge($message);
                echo 'Message has been acknowledged';
            }
        });
    }
}
