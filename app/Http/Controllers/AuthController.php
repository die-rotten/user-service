<?php

namespace App\Http\Controllers;

use App\Transformers\UserTransformer;
use Illuminate\Support\Facades\Auth;

class AuthController extends RestController
{
    protected $transformer = UserTransformer::class;

    /**
     * Who am i docs.
     *
     * @return void
     */
    public function whoami()
    {
        try {
            $user = Auth::user();
            $resource = $this->generateItem($user);

            return $this->sendResponse($resource);
        } catch (\Exception $e) {
            return $this->sendInternalErrorResponse($e->getMessage());
        }
    }
}
