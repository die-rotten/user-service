<?php

namespace App\Http\Controllers;

class IndexController extends Controller
{
    /**
     * @OA\Info(
     *   description="Microservice to handle all user-related.",
     *   title="Rotten/UserService",
     *   version="1.0.0",
     *   @OA\SecurityScheme(
     *     securityScheme="bearer",
     *     type="apiKey",
     *     name="Authorization",
     *     in="header",
     *   ),
     * )
     */
    public function index()
    {
        return response()->json([
            'version' => '1.0.0',
            'name' => 'Rotten/UserService',
            'description' => 'Microservice that responsible to handle all user activites',
        ]);
    }
}
