<?php

namespace App\Http\Controllers;

use App\Role;
use App\Services\RegistrationService;
use App\Transformers\UserTransformer;
use Illuminate\Http\Request;

class RegistrationController extends RestController
{
    protected $transformer = UserTransformer::class;

    /**
     * @OA\Post(
     *   path="/register",
     *   tags={"Users"},
     *   summary="Register a new user",
     *   operationId="users_register",
     *   @OA\RequestBody(
     *     description="Body request of register user",
     *     @OA\Schema(
     *       type="object",
     *       @OA\Property(
     *         property="fullName",
     *         type="string",
     *       ),
     *       @OA\Property(
     *         property="password",
     *         type="string",
     *       ),
     *       @OA\Property(
     *         property="confirmationPassword",
     *         type="string",
     *       ),
     *       @OA\Property(
     *         property="phoneNumber",
     *         type="string",
     *       ),
     *       @OA\Property(
     *         property="email",
     *         type="string",
     *       ),
     *       @OA\Property(
     *         property="passwordConfirmation",
     *         type="string",
     *       ),
     *       @OA\Property(
     *         property="isAuthor",
     *         type="boolean",
     *       ),
     *     ),
     *   ),
     *   @OA\Response(
     *     response=201,
     *     description="Succesful",
     *     @OA\JsonContent(ref="#/components/schemas/UserItemResponse")),
     *   ),
     *
     * @param Request $request
     * @param RegistrationService $registration_service
     * @return void
     */
    public function register(Request $request, RegistrationService $registration_service)
    {
        $this->validate($request, [
            'email' => 'required|unique:users',
            'password' => 'required',
            'confirmationPassword' => 'required|same:password',
            'fullName' => 'required',
            'phoneNumber' => 'required',
            'isAuthor' => 'required',
        ]);

        $role = $request->isAuthor ? Role::AUTHOR : Role::CUSTOMER;

        try {
            $data = [
                'email' => $request->email,
                'password' => app('hash')->make($request->password),
                'role_id' => $role,
                'personal' => [
                    'full_name' => $request->fullName,
                    'phone_number' => $request->phoneNumber,
                ],
            ];

            $user = $registration_service->register($data);
            $resource = $this->generateItem($user);

            return $this->sendResponse($resource, 201);
        } catch (\Exception $e) {
            return $this->sendInternalErrorResponse($e->getMessage());
        }
    }
}
