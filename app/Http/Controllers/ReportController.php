<?php

namespace App\Http\Controllers;

use App\Services\ReportService;

class ReportController
{
    /**
     * Return customers amount.
     *
     * @return void
     */
    public function getCustomersAmount(ReportService $report_service)
    {
        try {
            $amount = $report_service->getCustomersAmount();

            return response()->json(['amount' => $amount]);
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
    }
}
