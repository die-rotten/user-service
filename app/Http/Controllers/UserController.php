<?php

namespace App\Http\Controllers;

use App\Services\PersonalService;
use App\Services\UserService;
use App\Transformers\UserTransformer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class UserController extends RestController
{
    protected $transformer = UserTransformer::class;

    /**
     * @OA\Get(
     *   path="/users",
     *   tags={"Users"},
     *   summary="Return available users",
     *   operationId="users_index",
     *   @OA\Response(
     *     response=200,
     *     description="Succesful",
     *     @OA\JsonContent(ref="#/components/schemas/UserListResponse")),
     *   ),
     * )
     *
     * @param UserService $user_service
     * @return void
     */
    public function get(UserService $user_service)
    {
        try {
            $resource = $this->generateCollection($user_service->get());

            return $this->sendResponse($resource);
        } catch (\Exception $e) {
            return $this->sendInternalErrorResponse($e->getMessage());
        }
    }

    /**
     * @OA\Get(
     *   path="/users/{id}",
     *   tags={"Users"},
     *   summary="Return available a user",
     *   operationId="users_find",
     *   @OA\Parameter(
     *     name="User ID",
     *     in="path",
     *     description="User ID to find",
     *     required=true,
     *     @OA\Schema(
     *       type="integer",
     *     ),
     *   ),
     *   @OA\Response(
     *     response=200,
     *     description="Succesful",
     *     @OA\JsonContent(ref="#/components/schemas/UserItemResponse")),
     *   ),
     * )
     *
     *
     * @param [type] $id
     * @param UserService $user_service
     * @return void
     */
    public function find($id, UserService $user_service)
    {
        try {
            $user = $user_service->find($id);
            $resource = $this->generateItem($user);

            return $this->sendResponse($resource);
        } catch (ModelNotFoundException $e) {
            return $this->sendNotFoundErrorResponse($e->getMessage());
        } catch (\Exception $e) {
            return $this->sendInternalErrorResponse($e->getMessage());
        }
    }

    /**
     * @OA\Post(
     *   path="/users/change-password",
     *   tags={"Users"},
     *   summary="Update user password",
     *   operationId="users_change_password",
     *   security={{"bearer":{}}},
     *   @OA\RequestBody(
     *     description="Body request of change user password",
     *     @OA\Schema(
     *       type="object",
     *       @OA\Property(
     *         property="oldPassword",
     *         type="string",
     *       ),
     *       @OA\Property(
     *         property="newPassword",
     *         type="string",
     *       ),
     *       @OA\Property(
     *         property="newPasswordConfirmation",
     *         type="string",
     *       ),
     *     ),
     *   ),
     *   @OA\Response(
     *     response=200,
     *     description="Succesful",
     *     @OA\JsonContent(ref="#/components/schemas/UserItemResponse")),
     *   ),
     * )
     *
     * @param Request $request
     * @param UserService $user_service
     * @return void
     */
    public function changePassword(Request $request, UserService $user_service)
    {
        $this->validate($request, [
            'oldPassword' => 'required',
            'newPassword' => 'required',
            'newPasswordConfirmation' => 'required|same:newPassword',
        ]);

        try {
            $data = [
                'old_password' => $request->oldPassword,
                'password' => app('hash')->make($request->newPassword)
            ];

            $user = $user_service->updatePassword(Auth::user()->id, $data);
            $resource = $this->generateItem($user);

            return $this->sendResponse($resource);
        } catch (ModelNotFoundException $e) {
            return $this->sendNotFoundErrorResponse($e->getMessage());
        } catch (\Exception $e) {
            return $this->sendInternalErrorResponse($e->getMessage());
        }
    }

    /**
     * @OA\Patch(
     *   path="/users/{id}",
     *   tags={"Users"},
     *   summary="Update user personal data",
     *   operationId="users_update",
     *   @OA\Parameter(
     *     name="User ID",
     *     in="path",
     *     description="User ID to find",
     *     required=true,
     *     @OA\Schema(
     *       type="integer",
     *     ),
     *   ),
     *   @OA\RequestBody(
     *     description="Body request of update user",
     *     @OA\Schema(
     *       type="object",
     *       @OA\Property(
     *         property="email",
     *         type="string",
     *       ),
     *       @OA\Property(
     *         property="fullName",
     *         type="string",
     *       ),
     *     ),
     *   ),
     *   @OA\Response(
     *     response=200,
     *     description="Succesful",
     *     @OA\JsonContent(ref="#/components/schemas/UserItemResponse")),
     *   ),
     * )
     *
     *
     *
     * @param int $id
     * @param Request $request
     * @param PersonalService $personal_service
     * @return void
     */
    public function update($id, Request $request, PersonalService $personal_service)
    {
        $this->validate($request, [
            'fullName' => 'required',
            'phoneNumber' => 'required',
        ]);

        $address = !is_null($request->address) ? $request->address : '';

        try {
            $data = [
                'full_name' => $request->fullName,
                'phone_number' => $request->phoneNumber,
                'address' => $address,
            ];

            $user = $personal_service->update($id, $data)->user;
            $resource = $this->generateItem($user);

            return $this->sendResponse($resource);
        } catch (ModelNotFoundException $e) {
            return $this->sendNotFoundErrorResponse($e->getMessage());
        } catch (\Exception $e) {
            return $this->sendInternalErrorResponse($e->getMessage());
        }
    }
}
