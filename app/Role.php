<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $fillable = ['name'];

    /**
     * Constant value of ID with admin role.
     */
    const ADMIN = 1;

    /**
     * Constant value of ID with customer role.
     */
    const CUSTOMER = 2;

    /**
     * Constant value of ID with web service operator role.
     */
    const WEB_SERVICE_OPERATOR = 3;

    /**
     * Constant value of ID with author role.
     */
    const AUTHOR = 4;

    /**
     * Define relation with user model.
     *
     * @return void
     */
    public function users()
    {
        return $this->hasMany(User::class, 'role_id');
    }
}
