<?php

namespace App\Services;

use App\Personal;

class PersonalService
{
    /**
     * Update a personal data.
     *
     * @param int $id
     * @param array $data
     * @return void
     */
    public function update($id, $data)
    {
        try {
            $personal = Personal::where('user_id', $id)->first();
            $personal->update($data);

            return $personal;
        } catch (\Exception $e) {
            throw $e;
        }
    }
}
