<?php

namespace App\Services;

use App\User;
use Illuminate\Support\Facades\DB;

class RegistrationService
{
    /**
     * Register a new user.
     *
     * @param array $data
     * @return void
     */
    public function register($data)
    {
        try {
            $user = DB::transaction(function () use ($data) {
                $item = User::create($data);
                $item->personal()->create($data['personal']);

                return $item;
            });

            return $user;
        } catch (\Exception $e) {
            throw $e;
        }
    }
}
