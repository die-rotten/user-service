<?php

namespace App\Services;

use App\Role;
use App\User;

class ReportService
{
    /**
     * Return customers amount.
     *
     * @return void
     */
    public function getCustomersAmount()
    {
        return User::where('role_id', Role::CUSTOMER)->count();
    }
}
