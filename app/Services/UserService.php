<?php

namespace App\Services;

use App\User;
use Exception;
use Illuminate\Support\Facades\Hash;

class UserService
{
    /**
     * Return all users.
     *
     * @return void
     */
    public function get()
    {
        return User::with('personal')->get();
    }

    /**
     * Find a user.
     *
     * @param int $id
     * @return Model
     */
    public function find($id)
    {
        try {
            return User::findOrFail($id);
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * Update a user data.
     *
     * @param int $id
     * @param array $data
     * @return void
     */
    public function update($id, $data)
    {
        try {
            $user = $this->find($id);
            $user->update($data);

            return $user;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Update password, updates user password.
     *
     * @param int $id
     * @param array $data
     * @return void
     */
    public function updatePassword($id, $data)
    {
        try {
            $user = $this->find($id);

            if (!Hash::check($data['old_password'], $user->password)) {
                throw new Exception('Current password is wrong!');
            }

            $user->update($data);
            return $user;
        } catch (\Exception $e) {
            throw $e;
        }
    }
}
