<?php

namespace App\Transformers;

use App\Personal;
use League\Fractal\TransformerAbstract;

/**
 * @OA\Schema(
 *  title="PersonalObject",
 *  schema="PersonalObject",
 *  description="Respose object of Personal",
 *  @OA\Property(
 *    property="fullName",
 *    type="string",
 *  ),
 *  @OA\Property(
 *    property="phoneNumber",
 *    type="string",
 *  ),
 *
 * @OA\Schema(
 *  title="PersonalItemResponse",
 *  schema="PersonalItemResponse",
 *  description="Personal Item responses",
 *  @OA\Property(
 *    property="data",
 *    ref="#/components/schemas/PersonalObject"),
 *  )
 * )
 */
class PersonalTransformer extends TransformerAbstract
{
    /**
     * Transform personal model.
     *
     * @param Personal $personal
     * @return void
     */
    public function transform(Personal $personal)
    {
        return [
            'fullName' => $personal->full_name,
            'phoneNumber' => $personal->phone_number,
            'address' => $personal->address,
        ];
    }
}
