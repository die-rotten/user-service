<?php

namespace App\Transformers;

use App\Role;
use League\Fractal\TransformerAbstract;

/**
 * @OA\Schema(
 *  title="RoleObject",
 *  schema="RoleObject",
 *  description="Respose object of Role",
 *  @OA\Property(
 *    property="id",
 *    type="integer",
 *  ),
 *  @OA\Property(
 *    property="name",
 *    type="string",
 *  ),
 *
 * @OA\Schema(
 *  title="RoleItemResponse",
 *  schema="RoleItemResponse",
 *  description="Role Item responses",
 *  @OA\Property(
 *    property="data",
 *    ref="#/components/schemas/RoleObject"),
 *  )
 * )
 */
class RoleTransformer extends TransformerAbstract
{
    public function transform(Role $role)
    {
        return [
            'id' => $role->id,
            'name' => $role->name,
        ];
    }
}
