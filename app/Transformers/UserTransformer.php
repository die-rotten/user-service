<?php

namespace App\Transformers;

use App\User;
use League\Fractal\TransformerAbstract;

/**
 * @OA\Schema(
 *  title="UserObject",
 *  schema="UserObject",
 *  description="Respose object of User",
 *  @OA\Property(
 *    property="id",
 *    type="integer",
 *  ),
 *  @OA\Property(
 *    property="email",
 *    type="string",
 *  ),
 *  @OA\Property(
 *    property="personal",
 *    type="object",
 *    ref="#/components/schemas/PersonalObject",
 *  ),
 *  @OA\Property(
 *    property="role",
 *    type="object",
 *    ref="#/components/schemas/RoleObject",
 *  ),
 * ),
 *
 * @OA\Schema(
 *  title="UserListResponse",
 *  schema="UserListResponse",
 *  description="User list responses",
 *  @OA\Property(
 *    property="data",
 *    type="array",
 *    @OA\Items(ref="#/components/schemas/UserObject"),
 *  ),
 * ),
 *
 * @OA\Schema(
 *  title="UserItemResponse",
 *  schema="UserItemResponse",
 *  description="User Item responses",
 *  @OA\Property(
 *    property="data",
 *    ref="#/components/schemas/UserObject"),
 *  )
 * )
 */
class UserTransformer extends TransformerAbstract
{
    
    /**
     * Default model to include on response.
     *
     * @var array
     */
    protected $defaultIncludes = [
        'personal',
        'role',
    ];

    /**
     * Transform the model.
     *
     * @param User $user
     * @return void
     */
    public function transform(User $user)
    {
        return [
            'id' => $user->id,
            'email' => $user->email,
        ];
    }

    /**
     * Include personal model on response.
     *
     * @param User $user
     * @return void
     */
    public function includePersonal(User $user)
    {
        return $this->item($user->personal, new PersonalTransformer);
    }

    /**
     * Include role model on response.
     *
     * @param User $user
     * @return void
     */
    public function includeRole(User $user)
    {
        return $this->item($user->role, new RoleTransformer);
    }
}
