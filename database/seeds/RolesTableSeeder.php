<?php

use App\Role;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    public function run()
    {
        $data = [
            [
                'id' => 1,
                'name' => 'Admin',
            ],
            [
                'id' => 2,
                'name' => 'Customer',
            ],
            [
                'id' => 3,
                'name' => 'Web Service Operator',
            ],
            [
                'id' => 4,
                'name' => 'Author',
            ],
        ];

        foreach ($data as $item) {
            if (is_null(Role::find($item['id']))) {
                Role::create($item);
            }
        }
    }
}
