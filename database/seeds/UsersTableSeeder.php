<?php

use App\Role;
use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        $data = [
            [
                'id' => 1,
                'email' => 'admin@gmail.com',
                'password' => app('hash')->make('thegunners'),
                'role_id' => Role::ADMIN,
                'personal' => [
                    'full_name' => 'Admin Test',
                    'phone_number' => '087888056565',
                ],
            ],
            [
                'id' => 2,
                'email' => 'customer@gmail.com',
                'password' => app('hash')->make('thegunners'),
                'role_id' => Role::CUSTOMER,
                'personal' => [
                    'full_name' => 'Customer Test',
                    'phone_number' => '087888056565',
                ],
            ],
            [
                'id' => 3,
                'email' => 'ustabdulkhodir@gmail.com',
                'password' => app('hash')->make('thegunners'),
                'role_id' => Role::AUTHOR,
                'personal' => [
                    'full_name' => 'Ustad Abdul Khodir',
                    'phone_number' => '087789090000',
                ],
            ],
        ];

        foreach ($data as $item) {
            if (is_null(User::find($item['id']))) {
                $user = User::create([
                    'id' => $item['id'],
                    'email' => $item['email'],
                    'password' => $item['password'],
                    'role_id' => $item['role_id'],
                ]);

                $user->personal()->create($item['personal']);
            }
        }
    }
}
