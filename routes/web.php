<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/whoami', [
    'middleware' => 'auth',
    'uses' => 'AuthController@whoami',
]);

$router->post('/users/change-password', [
    'middleware' => 'auth',
    'uses' => 'UserController@changePassword',
]);

$router->get('/', 'IndexController@index');

$router->get('/users', 'UserController@get');
$router->get('/users/{id}', 'UserController@find');
$router->patch('/users/{id}', 'UserController@update');

$router->post('/register', 'RegistrationController@register');

$router->get('/reports/customers', 'ReportController@getCustomersAmount');
