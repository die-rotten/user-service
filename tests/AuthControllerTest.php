<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Passport\Client;

class AuthController extends TestCase
{
    use DatabaseMigrations;

    public function setUp(): void
    {
        parent::setUp();

        $seeder = app('DatabaseSeeder');
        $seeder->call('RolesTableSeeder');
        $seeder->call('UsersTableSeeder');

        $this->artisan('passport:install');
    }

    public function testAuth()
    {
        $client_id = 2;
        $client_secret = Client::findOrFail($client_id)->secret;

        $body = [
            'username' => 'customer@gmail.com',
            'password' => 'thegunners',
            'client_id' => $client_id,
            'client_secret' => $client_secret,
            'grant_type' => 'password',
        ];

        $response = $this->post('/oauth/token', $body);

        $response->assertResponseStatus(200);
    }
}
