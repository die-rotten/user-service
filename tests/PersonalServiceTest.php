<?php

use App\Services\PersonalService;
use App\Personal;
use Faker\Factory;
use Laravel\Lumen\Testing\DatabaseMigrations;

class PersonalServiceTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * @var Faker\Factory
     */
    protected $faker;

    /**
     * @var PersonalService
     */
    protected $personal_service_test;

    /**
     * Setup test.
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->personal_service_test = new PersonalService;
        $this->faker = Factory::create();

        $seeder = app('DatabaseSeeder');
        $seeder->call('RolesTableSeeder');
        $seeder->call('UsersTableSeeder');
    }

    public function testUpdateOnPersonalService()
    {
        $data = [
            'phone_number' => '0899000999',
            'full_name' => $this->faker->name,
            'address' => $this->faker->address,
        ];

        $personal = $this->personal_service_test->update(1, $data);

        $this->assertInstanceOf(Personal::class, $personal);
    }
}
