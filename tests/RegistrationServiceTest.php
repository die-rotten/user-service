<?php

use App\Role;
use App\Services\RegistrationService;
use App\User;
use Faker\Factory;
use Laravel\Lumen\Testing\DatabaseMigrations;

class RegistrationServiceTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * @var Faker\Factory
     */
    protected $faker;

    /**
     * @var RegistrationService
     */
    protected $registration_service_test;

    /**
     * Setup test.
     *
     * @return void
     */
    public function setUp() :void
    {
        parent::setUp();

        $this->registration_service_test = new RegistrationService;
        $this->faker = Factory::create();

        $seeder = app('DatabaseSeeder');
        $seeder->call('RolesTableSeeder');
        $seeder->call('UsersTableSeeder');
    }

    public function testRegisterOnRegistrationService()
    {
        $data = [
            'email' => $this->faker->email,
            'password' => $this->faker->word,
            'role_id' => Role::AUTHOR,
            'personal' => [
                'full_name' => $this->faker->name,
                'phone_number' => '08888899999',
            ],
        ];

        $user = $this->registration_service_test->register($data);

        $this->assertInstanceOf(User::class, $user);
    }
}
