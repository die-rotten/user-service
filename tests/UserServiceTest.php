<?php

use App\Role;
use App\Services\UserService;
use App\User;
use Faker\Factory;
use Laravel\Lumen\Testing\DatabaseMigrations;

class UserServiceTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * @var Faker\Factory
     */
    protected $faker;

    /**
     * @var UserService
     */
    protected $user_service_test;

    /**
     * Setup test.
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->user_service_test = new UserService;
        $this->faker = Factory::create();

        $seeder = app('DatabaseSeeder');
        $seeder->call('RolesTableSeeder');
        $seeder->call('UsersTableSeeder');
    }

    public function testGetOnUserService()
    {
        $users = $this->user_service_test->get();

        $this->assertTrue(count($users) > 0);
        $this->assertInstanceOf(User::class, $users[0]);
    }

    public function testUpdatePasswordOnUserService()
    {
        $user = $this->user_service_test->find(1);

        $data = [
            'old_password' => 'thegunners',
            'password' => $this->faker->name,
        ];

        $user = $this->user_service_test->updatePassword($user->id, $data);

        $this->assertNotNull($user);
    }

    public function testFindOnUserService()
    {
        $user = $this->user_service_test->find(1);

        $this->assertEquals(1, $user->id);
    }
}
